const User = require('../models/userModel');
const asyncErrorHandler = require('../middlewares/asyncErrorHandler');
const sendToken = require('../utils/sendToken');
const ErrorHandler = require('../utils/errorHandler');

// Register User
exports.registerUser = asyncErrorHandler(async (req, res, next) => {

    const { first_name, last_name, email, password } = req.body;

    const user = await User.create({
        first_name,
        last_name, 
        email,
        password,
    });

    sendToken(user, 201, res);

});

// Login User
exports.loginUser = asyncErrorHandler(async (req, res, next) => {
    const { email, password } = req.body;

    if(!email || !password) {
        return next(new ErrorHandler("Please Enter Email And Password", 400));
    }

    const user = await User.findOne({ email}).select("+password");

    if(!user) {
        return next(new ErrorHandler("Invalid Email or Password", 401));
    }

    const isPasswordMatched = await user.comparePassword(password);

    if(!isPasswordMatched) {
        return next(new ErrorHandler("Invalid Email or Password", 401));
    }

    sendToken(user, 201, res);
});

// Logout User
exports.logoutUser = asyncErrorHandler(async (req, res, next) => {
    res.cookie("token", null, {
        expires: new Date(Date.now()),
        httpOnly: false,
    });

    res.status(200).json({
        success: true,
        message: "Logged Out",
    });
});

// Get User Details
exports.getUserDetails = asyncErrorHandler(async (req, res, next) => {
    
    const user = await User.findById(req.user.id);

    res.status(200).json({
        success: true,
        user,
    });
});


// Update User Profile
exports.updateProfile = asyncErrorHandler(async (req, res, next) => {

    if (req.body.token) {
        const newUserData = req.body

        await User.findByIdAndUpdate(req.user.id, newUserData, {
            new: true,
            runValidators: true,
            useFindAndModify: true,
        });
        
    } else {
        const newUserData = req.body

        await User.findByIdAndUpdate(res.user.id, newUserData, {
            new: true,
            runValidators: true,
            useFindAndModify: true,
        });
    }    

    res.status(200).json({
        success: true,
    });
});