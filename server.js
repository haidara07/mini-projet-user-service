const app = require('./app');
const connectDatabase = require('./config/database');
const PORT = process.env.PORT;

connectDatabase();

app.listen(PORT, () => {
    console.log(`User Service --> Server running on ${process.env.USER_SERVICE_URL}`)
});
