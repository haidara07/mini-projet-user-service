const jwt = require('jsonwebtoken');
const User = require('../models/userModel');
const asyncErrorHandler = require('./asyncErrorHandler');
require('dotenv').config({ path: './config/.env' })

exports.isAuthenticatedUser = asyncErrorHandler(async ( req, res, next) => {

    const token = req.body.token;
    if (!token) {
        token=res.cookies
    }

    if (!token) {
        return res.status(401).send({ error: "Please Login to Access" });  
    }

    const decodedData = jwt.verify(token, process.env.JWT_SECRET);
    req.user = await User.findById(decodedData.id);
    next();
});


exports.autControl = asyncErrorHandler(async ( req, res, next) => {

    const token = req.body.token;
    if (!token) {
        token=res.cookies
    }

    if (!token) {
        return res.status(401).send({ error: "Please Login to Access" });  
    }

    const decodedData = jwt.verify(token, process.env.JWT_SECRET);
    const user = await User.findById(decodedData.id);
    res.status(200).json({
        success: true,
        user: user
    });
});

module.exports.requireAuth = asyncErrorHandler(async ( req, res, next) => {

    let token = req.body.token;
    if (!token) {
        token=res.cookies
    }

    if (!token) {
        return res.status(401).send({ error: "Please Login to Access" });  
    }

    const decodedData = jwt.verify(token, process.env.JWT_SECRET);
    const user = await User.findById(decodedData.id);
    res.status(200).json({
        success: true,
        userId: user._id
    });
});