const express = require('express');
const { registerUser, loginUser, logoutUser, getUserDetails, updateProfile} = require('../controllers/userController');
const { isAuthenticatedUser, autControl } = require('../middlewares/auth');
const {requireAuth} = require('../middlewares/auth');

const router = express.Router();

router.route('/register').post(registerUser);
router.route('/login').post(loginUser);
router.route('/logout').get(logoutUser);

router.route('/me').get(isAuthenticatedUser, getUserDetails);

router.route('/me/update').put(isAuthenticatedUser, updateProfile);

router.route('/me/auth').get(autControl);


router.route('/jwtid').get(requireAuth);


module.exports = router;